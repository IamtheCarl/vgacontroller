`timescale 1ns / 1ps

module VGASimulation;
    reg clk;
    
    initial clk <= 'b0;
    
    initial
    begin
        forever
            #1
            clk = ~clk;
    end
    
    wire Hsync;
    wire Vsync;
    
    VGASynthesis synth(.clk(clk), .vgaRed(), .vgaGreen(), .vgaBlue(), .Hsync(Hsync), .Vsync(Vsync));            
endmodule
