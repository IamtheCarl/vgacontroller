`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2018 11:06:06 PM
// Design Name: 
// Module Name: VGASynthesis
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGADriver(
        clk,
        Hsync,
        Vsync,
        video_X,
        video_Y,
        video_active
    );
    
    input clk;
    output reg Hsync;
    output reg Vsync;
    
    output [$clog2(screen_width ) - 1: 0] video_X;
    output [$clog2(screen_height) - 1: 0] video_Y;
    
    wire xActive;
    wire yActive;
    
    output wire video_active;
    
    initial
    begin
        Hsync <= 'b0;
        Vsync <= 'b0;
    end
    
    parameter screen_width = 'd640; // 640
    parameter horizontal_frontporch = 'd16; // 16  
    parameter horizontal_backporch = 'd48; // 48
    parameter horizontal_sync_pulse = 'd96; // 96
    localparam frame_width = horizontal_sync_pulse + horizontal_frontporch + screen_width + horizontal_backporch;
    
    parameter screen_height = 'd480; // 480
    parameter vertical_frontporch = 'd10; // 12
    parameter vertical_backporch = 'd35; // 35
    parameter vertical_sync_pulse = 'd2; // 2
    
    localparam frame_height = vertical_sync_pulse + vertical_frontporch + screen_height + vertical_backporch;    
    localparam total_pixels = screen_width * screen_height;

    reg [$clog2(frame_width) - 1: 0] scan_X;
    reg [$clog2(frame_height) - 1:0] scan_Y;

    assign xActive = scan_X > horizontal_sync_pulse + horizontal_frontporch && scan_X < horizontal_sync_pulse + horizontal_frontporch + screen_width;
    assign yActive = scan_Y > vertical_sync_pulse + vertical_frontporch && scan_Y < vertical_sync_pulse + vertical_frontporch + screen_height;
    assign video_active = xActive && yActive;

    assign video_X = (video_active) ? (scan_X - (horizontal_sync_pulse + horizontal_frontporch) - 1):'h0;
    assign video_Y = (video_active) ? (scan_Y - (vertical_sync_pulse + vertical_frontporch) - 1):'h0;
    
    initial
    begin
        scan_X <= 'd0;
        scan_Y <= 'd0;
    end
    
    always @ (posedge clk)
    begin
        if (scan_X < frame_width)
            scan_X <= scan_X + 'd1;
        else
        begin
            scan_X <= 0;
            if (scan_Y >= frame_height)
                scan_Y <= 'd0;
            else
                scan_Y <= scan_Y + 1;
        end
        
        if (scan_X < horizontal_sync_pulse)
            Hsync <= 'b1;
        else
            Hsync <= 'b0;
            
        if (scan_Y < vertical_sync_pulse)
            Vsync <= 'b1;
        else
            Vsync <= 'b0;
    end
    
endmodule