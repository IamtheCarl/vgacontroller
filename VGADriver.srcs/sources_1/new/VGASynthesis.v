`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/17/2018 11:06:06 PM
// Design Name: 
// Module Name: VGASynthesis
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module VGASynthesis(
        clk,
        vgaRed,
        vgaGreen,
        vgaBlue,
        Hsync,
        Vsync
    );
    
    input clk;
    output [3:0] vgaRed;
    output [3:0] vgaGreen;
    output [3:0] vgaBlue;
    output Hsync;
    output Vsync;
    
    wire video_clk;
    wire [9:0] video_X;
    wire [8:0] video_Y;

    wire video_active;
    
    wire [11:0] data;

    clock_provider clocks(.clk_in(clk), .video_clk(video_clk));
    VGADriver main_driver(.clk(video_clk), .Hsync(Hsync), .Vsync(Vsync), .video_X(video_X), .video_Y(video_Y), .video_active(video_active));
    
    BackgroundROM background(
        .clk(video_clk),
        .enabled(video_active),
        .address_x(video_X),
        .address_y(video_Y),
        .data(data)
    );
    
    assign vgaRed   = data[11:8];
    assign vgaGreen = data[7:4];
    assign vgaBlue  = data[3:0];
    
endmodule

