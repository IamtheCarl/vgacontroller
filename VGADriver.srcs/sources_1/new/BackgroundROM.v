module BackgroundROM(
    clk,
    enabled,
    address_x,
    address_y,
    data
);
    input clk;
    input enabled;
    input [9:0] address_x;
    input [8:0] address_y;
    output reg [11:0] data;

    (* rom_style = "Block" *)

    reg [11:0] image [320*240-1:0];
    
     // Image provided by Wiki Media Commons.
     // https://commons.wikimedia.org/wiki/File:Abandoned_train_station_Pankow_Heinersdorf.jpg
    initial
        $readmemh("image.hex_data", image);

    always @(posedge clk)
    if (enabled)
        data <= image[address_y[8:1] * 'd320 + address_x[9:1]];
    else
        data <= 'hz;
 

endmodule
