-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
-- Date        : Thu Sep 20 00:03:11 2018
-- Host        : james-Lenovo running 64-bit Ubuntu 16.04.5 LTS
-- Command     : write_vhdl -force -mode synth_stub
--               /home/james/Verilog/VGADriver/VGADriver.srcs/sources_1/ip/clock_provider/clock_provider_stub.vhdl
-- Design      : clock_provider
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35ticpg236-1L
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clock_provider is
  Port ( 
    video_clk : out STD_LOGIC;
    clk_in : in STD_LOGIC
  );

end clock_provider;

architecture stub of clock_provider is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "video_clk,clk_in";
begin
end;
