// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Thu Sep 20 00:03:11 2018
// Host        : james-Lenovo running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/james/Verilog/VGADriver/VGADriver.srcs/sources_1/ip/clock_provider/clock_provider_stub.v
// Design      : clock_provider
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35ticpg236-1L
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clock_provider(video_clk, clk_in)
/* synthesis syn_black_box black_box_pad_pin="video_clk,clk_in" */;
  output video_clk;
  input clk_in;
endmodule
